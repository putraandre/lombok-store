from flask import Flask, render_template, request, url_for, request, redirect
from flask_fontawesome import FontAwesome

app = Flask(__name__)
fa = FontAwesome(app)

# route index
@app.route('/', methods=['GET', 'POST'])
def home():
  return render_template('index.html')

@app.route('/keranjang', methods=['GET', 'POST'])
def keranjang():
  return render_template('keranjang.html')

@app.route('/product', methods=['GET', 'POST'])
def product():
  return render_template('product.html')

@app.route('/store', methods=['GET', 'POST'])
def store():
  return render_template('store.html')

@app.route('/signin', methods=['GET', 'POST'])
def signin():
  return render_template('signin.html')

@app.route('/signup', methods=['GET', 'POST'])
def signup():
  return render_template('signup.html')

@app.route('/admin', methods=['GET', 'POST'])
def admin():
  return render_template('/admin/dashboard.html')

if __name__ == '__main__':
  app.run(debug=True)